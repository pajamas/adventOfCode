with open("input","r") as f:
    lines = f.readlines()
lines = [int(x.strip()) for x in lines]


amount = 0

for i in range(len(lines)):
    if lines[i-1] < lines[i] and i > 0:
        amount += 1

print(amount)

"""
sums = []

for i in range(len(lines)):
    if 1<i<len(lines)-2:
        sums.append(lines[i]+lines[i+1]+lines[i+2])

amount2 = 0

for i in range(len(sums)):
    if i>0 and sums[i]>sums[i-1]:
        amount2 += 1

print(amount2)
"""

amount3 = 0

for i in range(len(lines)):
    if i>3 and lines[i] > lines[i-3]:
        amount3 += 1

print(amount3)
