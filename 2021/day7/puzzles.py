with open("input","r") as f:
    nums = f.read()

nums = [int(num) for num in nums.strip().split(",")]

def align(nums):
    fuelamounts = []
    for num in range(min(nums),max(nums)):
        fuel = 0
        for crab in nums:
            fuel += abs(crab-num)
        fuelamounts.append(fuel)
    return min(fuelamounts)

def align2(nums):
    fuelamounts = []
    for num in range(min(nums),max(nums)):
        fuel = 0
        for crab in nums:
            fuel += sum([i for i in range(abs(crab-num)+1)])
        fuelamounts.append(fuel)
        if len(fuelamounts)>2:
            if fuelamounts[-1] > fuelamounts[-2] < fuelamounts[-3]:
                return min(fuelamounts)

print(align(nums))
print(align2(nums))
