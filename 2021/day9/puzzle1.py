with open("input.txt") as f:
    lines = f.readlines()
lines = [line.strip() for line in lines if line.strip()]
lines = [[int(num) for num in line] for line in lines]

lows = []
for row_num, row in enumerate(lines):
    for col_num, num in enumerate(lines[row_num]):
        
        if row_num<len(lines)-1 and lines[row_num+1][col_num]<=num:
            continue
        if row_num>0 and lines[row_num-1][col_num]<=num:
            continue
        if col_num>0 and lines[row_num][col_num-1]<=num:
            continue
        if col_num<len(lines[0])-1 and lines[row_num][col_num+1]<=num:
            continue
        lows.append(num)
        
print(sum(lows) + len(lows))
