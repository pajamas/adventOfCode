with open("input.txt") as f:
    lines = f.readlines()
lines = [line.strip() for line in lines if line.strip()]
lines = [[int(num) for num in line] for line in lines]

basins = [[-1 for num in line] for line in lines]
basin_sizes = [0]

def fill(x,y,basin_num):
    global lines
    global basins
    global basin_sizes
    if x == len(lines[0]) or x == -1:
        return False
    if y == len(lines) or y == -1:
        return False
    if basins[y][x] != -1:
        return False
    if lines[y][x] == 9:
        return False
    basins[y][x] = basin_num
    basin_sizes[basin_num] += 1
    fill(x+1,y,basin_num)
    fill(x-1,y,basin_num)
    fill(x,y+1,basin_num)
    fill(x,y-1,basin_num)
    return True

basin_num = 0
for row_num, _ in enumerate(lines):
    for col_num, _ in enumerate(lines[row_num]):
        if basins[row_num][col_num] != -1:
            continue
        if fill(col_num, row_num, basin_num):
            basin_sizes.append(0)
            basin_num += 1
        
basin_sizes.sort()
print(f"Result: {basin_sizes[-1]*basin_sizes[-2]*basin_sizes[-3]}")
