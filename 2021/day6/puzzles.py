with open("input","r") as f:
    nums = f.read()

nums = [int(num) for num in nums.strip().split(",")]

def simulate(days,nums):
    for day in range(days):
        appendlist = []
        for i,num in enumerate(nums):
            if num == 0:
                nums[i] = 6
                appendlist.append(8)
            else:
                nums[i] -= 1
        nums += appendlist
    return len(nums)

print(simulate(80,nums.copy()))

def group(nums):
    groups = [0 for i in range(9)]
    for i in range(9):
        groups[i] = nums.count(i)
    return groups

def count(days,groups):
    for i in range(days):
        num = groups.pop(0)
        groups[6] += num
        groups.append(num)

    amount = 0
    for i in range(9):
        amount += groups[i]
    return amount


print(count(80,group(nums)))
print(count(256,group(nums)))

