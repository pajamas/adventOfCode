with open("input","r") as f:
    lines = f.readlines()

lines = [line.strip().split(" | ") for line in lines]

amount = 0
lengths = [2,3,4,7]
for line in lines:
    outvals = line[1].split(" ")
    for val in outvals:
        if len(val) in lengths:
            amount += 1
print(amount)


amount = 0
for line in lines:
    patterns = [''.join(sorted(pattern)) for pattern in line[0].split(" ")]
    outvals = [''.join(sorted(val)) for val in line[1].split(" ")]
    nums = ["" for i in range(10)]
    i = 0
    while "" in nums:
        pattern = patterns[i%len(patterns)]
        zeroornine = False
        if len(pattern) == 2:
            nums[1] = pattern
            i += 1
            continue
        if len(pattern) == 3:
            nums[7] = pattern
            i += 1
            continue
        if len(pattern) == 4:
            nums[4] = pattern
            i += 1
            continue
        if len(pattern) == 7:
            nums[8] = pattern
            i += 1
            continue
        if len(pattern) == 6 and nums[7]:
            for char in nums[7]:
                if char not in pattern:
                    nums[6] = pattern
                    zeroornine = False
                    break
                else:
                    zeroornine = True
        if zeroornine and nums[4]:
            for char in nums[4]:
                if char not in pattern:
                    nums[0] = pattern
                    break
            if nums[0] != pattern:
                nums[9] = pattern
                i += 1
                continue
        upright = ""
        if nums[6] and nums[1]:
            for char in "abcdefg":
                if char in nums[1] and char not in nums[6]:
                    upright = char
                    break
        fiveortwo = False
        if len(pattern) == 5 and nums[7]:
            for char in nums[7]:
                if char not in pattern:
                    fiveortwo = True
                    break
            if fiveortwo and upright:
                if upright not in pattern:
                    nums[5] = pattern
                else:
                    nums[2] = pattern
            if upright and not fiveortwo:
                nums[3] = pattern
        i += 1
                
    outnum = ""
    for val in outvals:
        outnum += str(nums.index(val))
    amount += int(outnum)
print(amount)
