with open("input","r") as f:
    lines = f.readlines()


# convert lines to usable format
nums = lines[0].strip().split(",")
nums = [int(num) for num in nums]

del lines[0:2]
lines = [line.strip().split(" ") for line in lines]
for i in range(len(lines)):
    lines[i] = [int(num) for num in lines[i] if num.isnumeric()]
#print(lines)

boards = [[[],[],[],[],[]] for i in range(int(len(lines)/6)+1)]

for i in range(int(len(lines)/6)+1):
    for j in range(5):
        boards[i][j] = [int(num) for num in lines[6*i+j]]
        boards[i][j] = [[num,False] for num in boards[i][j]]



def checkForWin(board):
    # rows
    for row in board:
        bools = [num[1] for num in row]
        if False in bools: continue
        else: return board
    #columns
    for i in range(5):
        bools = [board[x][i][1] for x in range(5)]
        if False in bools: continue
        else: return board
    return False

def bingo():
    for num in nums:
        # mark numbers that have been called
        for i in range(len(boards)):
            for j in range(5):
                if [num,False] in boards[i][j]:
                    boards[i][j][boards[i][j].index([num,False])][1] = True

        # check for win, calculate score
        for board in boards:
            if checkForWin(board):
                score = 0
                for row in board:
                    for number in row:
                        if number[1] == False:
                            score += number[0]
                score *= num
                for row in board:
                    print(row)
                return(score)

    return(False)

print(f"Score: {bingo()}")

def loseBingo():
    for num in nums:
        # mark numbers that have been called
        for i in range(len(boards)):
            for j in range(5):
                if [num,False] in boards[i][j]:
                    boards[i][j][boards[i][j].index([num,False])][1] = True

        # check for win
        for board in boards:
            # remove winning boards
            # if only one left, calculate score
            if checkForWin(board):
                if len(boards)>1:
                    boards.remove(board)
                else:
                    score = 0
                    for row in board:
                        for number in row:
                            if number[1] == False:
                                score += number[0]
                    score *= num
                    return score

    return(False)

print(f"Worst board score: {loseBingo()}")
