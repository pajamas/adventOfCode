with open("input","r") as f:
    lines = f.readlines()

splitlines = [line.strip().split(" ") for line in lines]

h = 0
d = 0

for line in splitlines:
    a = int(line[1])
    if line[0] == "forward":
        h += a
    elif line[0] == "down":
        d += a
    elif line[0] == "up":
        d -= a

print(h*d)

aim = 0
pos = 0
dep = 0

for line in splitlines:
    a = int(line[1])
    if line[0] == "forward":
        pos += a
        dep += a*aim
    elif line[0] == "down":
        aim += a
    elif line[0] == "up":
        aim -= a

print(pos*dep)
