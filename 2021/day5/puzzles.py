with open("input","r") as f:
    lines = f.readlines()

for i in range(len(lines)):
    lines[i] = lines[i].strip().split(" -> ")
    lines[i][0] = [int(num) for num in lines[i][0].split(",")]
    lines[i][1] = [int(num) for num in lines[i][1].split(",")]

def only_hv(lines: list) -> list:
    out = []
    for line in lines:
        if line[0][0] == line[1][0] or line[0][1] == line[1][1]:
            out.append(line)
    lines = out
    return lines

def list_all_points(lines: list) -> list:
    newlines = []
    for i,line in enumerate(lines):
        x1 = line[0][0]
        x2 = line[1][0]
        y1 = line[0][1]
        y2 = line[1][1]

        if x1 < x2:
            xlist = [i for i in range(x1,x2+1)]
        elif x1 == x2:
            xlist = [x1 for i in range(min(y1,y2),max(y1,y2)+1)]
        elif x2 < x1:
            xlist = [i for i in range(x1,x2-1,-1)]
        if y1 < y2:
            ylist = [i for i in range(y1,y2+1)]
        elif y1 == y2:
            ylist = [y1 for i in range(min(x1,x2),max(x1,x2)+1)]
        elif y2 < y1:
            ylist = [i for i in range(y1,y2-1,-1)]

        line = [(x,y) for x,y in zip(xlist, ylist)]
        newlines.append(line)

    lines = newlines
    return lines

def count_overlap_points(lines: list) -> int:
    amount = 0
    d = {}
    for x in range(0,1000):
        for y in range(0,1000):
            d[(x,y)] = 0
    for points in lines:
        for point in points:
            d[point] += 1
    for point in d:
        if d[point]>1:
            amount += 1
    return amount

print(count_overlap_points(list_all_points(only_hv(lines))))
print(count_overlap_points(list_all_points(lines)))
