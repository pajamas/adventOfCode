with open("input","r") as f:
    lines = f.readlines()

lines = [line.strip() for line in lines]

result1 = ""
result2 = ""
zeroes = [0 for i in range(len(lines[0]))]
ones = [0 for i in range(len(lines[0]))]
for line in lines:
    for i in range(len(line)):
        if line[i] == "0":
            zeroes[i] += 1
        else:
            ones[i] += 1

for i in range(len(zeroes)):
    if zeroes[i]>ones[i]:
        result1 += "0"
        result2 += "1"
    else:
        result1 += "1"
        result2 += "0"

print(int(result1,2)*int(result2,2))

def rating(mostorleast,nums,i):
    zeroes = 0
    ones = 0
    for line in nums:
        if line[i] == "0":
            zeroes += 1
        else:
            ones += 1
    
    if zeroes>ones:
        if mostorleast == "m": criteria = "0"
        else: criteria = "1"
    elif zeroes==ones:
        if mostorleast == "m": criteria = "1"
        else: criteria = "0"
    else:
        if mostorleast == "m": criteria = "1"
        else: criteria = "0"
    nums = [line for line in nums if line[i]==criteria]
    if len(nums) == 1:
        return nums[0]
    else:
        return rating(mostorleast,nums,i+1)

a = rating("m",lines,0)
b = rating("l",lines,0)

print(int(a,2)*int(b,2))
