use std::fs;

fn main() {
    let content = fs::read_to_string("input.txt")
                     .expect("Couldn't open file");
    let elves: Vec<&str> = content.split("\n\n").collect();

    let mut most_calories = 0;

    for elf in elves {
        let sum = elf.lines()
            .map(|num| num.parse::<u32>().unwrap())
            .sum();
        if sum > most_calories {
            most_calories = sum;
        }
    }
    println!("Most calories: {}", most_calories);
}
