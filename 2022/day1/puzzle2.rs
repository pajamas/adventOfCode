use std::fs;

fn main() {
    let content = fs::read_to_string("input.txt")
                     .expect("Couldn't open file");
    let elves = content.split("\n\n");

    let mut top_3 = [0,0,0];

    for elf in elves {
        // sum == how many calories elf is carrying
        let sum: i32 = elf.lines()
            .map(|num| num.parse::<i32>().unwrap())
            .sum();
        // is sum bigger than smallest number in top 3
        let min: &mut _ = top_3.iter_mut().min().unwrap();
        if sum > *min {
            // put it in top 3
            *min = sum;
        }
    }
    println!("Total: {}", top_3.iter().sum::<i32>());
}
