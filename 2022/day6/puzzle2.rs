use std::fs;

fn duplicates(s: &str) -> Option<char> {
    return s.chars().enumerate().find_map(|(i, c)| 
            s.chars()
                .skip(i+1)
                .find(|ch| &c == ch)
    );
}

fn main() {
    let window_size = 14;
    let content = fs::read_to_string("input.txt")
        .expect("Could not read file");

    let mut i = 0;
    loop {
        let window = &content[i..i+window_size];
        match duplicates(window) {
            None => break,
            _ => i += 1
        }
    }
    println!("{}", i + window_size)
}
