use std::fs;

fn main() {
    let content = fs::read_to_string("input.txt")
        .expect("Could not read file");

    let mut parts = content.split("\n\n");
    let crate_stacks: Vec<&str> = parts.next().unwrap().lines().collect();
    let moves_str = parts.next().unwrap();
    let mut crates: Vec<Vec<&str>> = vec![vec!["";0];9];

    for i in 0..crate_stacks.len()-1 {
        for j in 0..9 {
            let char_index = 1 + 4*j;
            if &crate_stacks[crate_stacks.len()-2-i][char_index..char_index+1] == " " {
                continue;               
            }
            crates[j].push(&crate_stacks[crate_stacks.len()-2-i][char_index..char_index+1]);
        }
    }

    let lines: Vec<&str> = moves_str.split("\n").collect();
    let moves = lines.iter()
        .filter(|s| !s.is_empty())
        .map(|line| 
            line.split(' ')
            .skip(1)
            .step_by(2)
            .map(|s| s.parse::<i32>().unwrap())
            .collect::<Vec<_>>()
        );
    
    for line in moves {
        let amount = line[0];
        let source = line[1] as usize - 1;
        let target = line[2] as usize - 1;
        for i in 0..amount {
            let c = crates[source][crates[source].len()-((amount - i) as usize)];
            crates[target].push(c);
        }
        for _ in 0..amount {
            crates[source].pop();
        }
    }

    let mut result: String = "".to_string();

    for mut c in crates {
        result.push_str(c.pop().unwrap());
    }

    println!("Top crates: {}", result);
}