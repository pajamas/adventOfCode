use std::fs;
use std::collections::HashMap;

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Could not read file");
    let lines = contents.lines().filter(|s| !s.is_empty());

    let mut score = 0;

    let decrypted = HashMap::from([
        ("A","rock"),
        ("B","paper"),
        ("C","scissors"),
        ("X","rock"),
        ("Y","paper"),
        ("Z","scissors"),
    ]);

    let defeats = HashMap::from([
        ("rock", "scissors"),
        ("paper", "rock"),
        ("scissors", "paper")
    ]);

    let scores = HashMap::from([
        ("rock", 1),
        ("paper", 2),
        ("scissors", 3)
    ]);

    for line in lines {
        let me = decrypted.get(&line[2..3]).unwrap();
        let opp = decrypted.get(&line[0..1]).unwrap();

        score += scores.get(me).unwrap();

        if me == opp {
            score += 3;
        }
        else if defeats.get(me).unwrap() == opp {
            score += 6;
        }
    }
    
    println!("Total score: {}", score);
}
