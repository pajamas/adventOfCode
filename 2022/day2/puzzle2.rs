use std::fs;
use std::collections::HashMap;

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Could not read file");
    let lines = contents.lines().filter(|s| !s.is_empty());

    let mut score = 0;

    let decrypted = HashMap::from([
        ("A","rock"),
        ("B","paper"),
        ("C","scissors")
    ]);

    let defeats = HashMap::from([
        ("rock","scissors"),
        ("paper", "rock"),
        ("scissors", "paper")
    ]);

    let defeated_by = HashMap::from([
        ("scissors", "rock"),
        ("rock", "paper"),
        ("paper", "scissors")
    ]);

    let scores = HashMap::from([
        ("rock", 1),
        ("paper", 2),
        ("scissors", 3)
    ]);

    for line in lines {
        let opp = decrypted.get(&line[0..1]).unwrap();

        let round_result = &line[2..3];

        if round_result == "Y" {
            score += 3;
            score += scores.get(opp).unwrap();
        } else if round_result == "Z" {
            score += 6;
            let me = defeated_by.get(opp).unwrap();
            score += scores.get(me).unwrap();
        } else if round_result == "X" {
            let me = defeats.get(opp).unwrap();
            score += scores.get(me).unwrap();
        }
    }
    
    println!("Total score: {}", score);
}
