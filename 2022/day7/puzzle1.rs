fn ls<'a>(content: &'a str, dir: &str, i: usize) -> impl Iterator<Item = &'a str> {
    let lines = content.lines().skip(i);
    let start = lines.clone().position(|line| line == ["$ cd ", dir].join("")).unwrap() + 2;
    let len = lines.clone().skip(start).position(|line| line.contains("$ cd ")).unwrap_or(usize::MAX);
    lines.skip(start).take(len)
}

fn size_of(content: &str, directories: &Vec<(usize, &str)>, name: &str, i: usize, hwfm_child: bool) -> usize {
    let mut size: usize = 0;
    let dir_content = ls(&content, name, i);
    
    for line in dir_content {
        if &line[0..3] == "dir" {
            let mut dirs = directories.iter();
            let index = dirs.clone().skip(
                dirs.position(|dir| dir.0 >= i).unwrap()
            ).position(|dir| dir.1 == name).unwrap();
            size += size_of(content, directories, &line[4..], index, name == "hwfm" || hwfm_child);
        } else {
            size += line.split(" ").next().unwrap().parse::<usize>().unwrap();
        }
    }
    if name == "hwfm" || hwfm_child {
        println!("{i} size of {name}: {size}");
    }
    size
}

fn main() {
    let content = include_str!("input.txt");

    let directories = content.lines()
        .enumerate()
        .filter(|(_, line)|
            line.contains("$ cd ") && line != &"$ cd ..")
        .map(|(i, line)| (i, &line[5..]))
        .collect::<Vec<(_,&str)>>();

    
    let mut sum: usize = 0;
    for directory in &directories {
        let size: usize = size_of(&content, &directories, directory.1, directory.0, false);
        if directory.1 == "hwfm" {
            println!("hwfm was {size} bytes");
        }
        if size < 100000 {
            let name = directory.1;
            let i = directory.0;
            println!("{i} {name} was less than 100000 bytes");
            sum += size;
        }
    }
    println!("{sum}");
}