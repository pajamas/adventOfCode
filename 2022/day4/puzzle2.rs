use std::fs;

fn contains(start: i32, end: i32, num: i32) -> bool {
    return start <= num && end >= num;
}

fn main() {
    let content = fs::read_to_string("input.txt")
        .expect("Could not read file");
    let lines = content.lines();

    let mut count = 0;

    for line in lines {
        let parts: Vec<&str> = line.split(",").collect();
        let first_elf: Vec<i32> = parts[0].split("-")
            .map(|s| s.parse::<i32>().unwrap())
            .collect();
        let second_elf: Vec<i32> = parts[1].split("-")
            .map(|s| s.parse::<i32>().unwrap())
            .collect();
        let range1 = first_elf[0]..first_elf[1];
        let range2 = second_elf[0]..second_elf[1];

        if contains(range1.start, range1.end, range2.start) {
            count += 1;
        } else if contains(range1.start, range1.end, range2.end) {
            count += 1;
        } else if contains(range2.start, range2.end, range1.start) {
            count += 1;
        } else if contains(range2.start, range2.end, range1.end) {
            count += 1;
        }
    }

    println!("Total: {}", count);
}
