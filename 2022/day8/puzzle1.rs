use std::fs;

fn visible_top(nums: &Vec<Vec<i32>>, y: usize, x: usize) -> bool{
    let num = nums[y][x];
    for i in 0..y {
        if nums[i][x] >= num {
            return false;
        }
    }
    true
}

fn visible_bottom(nums: &Vec<Vec<i32>>, y: usize, x: usize) -> bool {
    let num = nums[y][x];
    for i in y+1..nums.len() {
        if nums[i][x] >= num {
            return false;
        }
    }
    true
}

fn visible_right(nums: &Vec<Vec<i32>>, y: usize, x: usize) -> bool {
    let num = nums[y][x];
    for i in x+1..nums[y].len() {
        if nums[y][i] >= num {
            return false;
        }
    }
    true
}

fn visible_left(nums: &Vec<Vec<i32>>, y: usize, x: usize) -> bool {
    let num = nums[y][x];
    for i in 0..x {
        if nums[y][i] >= num {
            return false;
        }
    }
    true
}

fn main() {
    let content = fs::read_to_string("input.txt")
        .expect("Could not read file");

    let nums = content.lines().map(
        |line| line.chars().map(
            |c| (c as u8 - '0' as u8) as i32
            ).collect::<Vec<i32>>()
        ).collect::<Vec<Vec<i32>>>();

    let mut sum = 0;
    for i in 0..nums.len() {
        for j in 0..nums[0].len() {
            if  visible_top(&nums, i, j) ||
                visible_right(&nums, i, j) ||
                visible_bottom(&nums, i, j) ||
                visible_left(&nums, i, j) {
                sum += 1;
            }
        }
    }
    println!("{sum}");
}
