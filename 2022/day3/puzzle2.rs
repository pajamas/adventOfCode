use std::fs;

fn item_priority(letter: char) -> i32 {
    let mut priority = letter as i32 - 'a' as i32 + 1;
    if priority < 1 {
        priority += 26 + 32;
    }
    return priority;
}

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Could not read file");
    let lines: Vec<&str> = contents.lines().collect();

    let mut sum = 0;

    for group_num in 0..lines.len()/3 {
        let elves = &lines[group_num*3..group_num*3+3];
        for letter in elves[0].chars() {
            if elves[1].contains(letter) && elves[2].contains(letter) {
                sum += item_priority(letter);
                break;
            }
        }
    }

    println!("Sum of priorities: {}", sum);
}
