use std::fs;

fn item_priority(letter: char) -> i32 {
    let mut priority = letter as i32 - 'a' as i32 + 1;
    if priority < 1 {
        priority += 26 + 32;
    }
    return priority;
}

fn main() {
    let contents = fs::read_to_string("input.txt")
        .expect("Could not read file");
    let lines = contents.lines();

    let mut sum = 0;

    for line in lines {
        let halfpoint = line.len() / 2;
        let part1 = &line[0..halfpoint];
        let part2 = &line[halfpoint..line.len()];
        for letter in part1.chars() {
            if part2.contains(letter) {
                sum += item_priority(letter);
                break;
            }
        }
    }

    println!("Sum of priorities: {}", sum);
}
