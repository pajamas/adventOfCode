def cubes_needed(game):
    cube_sets = game.split(": ")[1].split(";")
    cube_sets = [cube_set.split(",") for cube_set in cube_sets]

    needed_amounts = {"blue": 0, "red": 0, "green": 0}

    for cube_set in cube_sets:
        color_amounts = {color: 0 for color in needed_amounts}
        for subset in cube_set:
            subset = subset.strip().split()
            color_amounts[subset[1]] += int(subset[0])
        for color, amount in color_amounts.items():
            if amount > needed_amounts[color]:
                needed_amounts[color] = amount

    return needed_amounts

def game_power(color_amounts):
    power = 1
    for amount in color_amounts.values():
        power *= amount
    return power

def game_powers(games):
    return [game_power(cubes_needed(game)) for game in games]

with open("input") as f:
    games = [line.strip() for line in f.readlines()]

print(sum(game_powers(games)))
