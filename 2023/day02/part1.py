def is_possible(game):
    cube_sets = game.split(": ")[1].split(";")
    cube_sets = [cube_set.split(",") for cube_set in cube_sets]

    max_amounts = {"blue": 14, "red": 12, "green": 13}

    for cube_set in cube_sets:
        color_amounts = {color: 0 for color in max_amounts}
        for subset in cube_set:
            subset = subset.strip().split()
            color_amounts[subset[1]] += int(subset[0])
        for color, amount in color_amounts.items():
            if amount > max_amounts[color]:
                return False

    return True

def game_id(game):
    return int(game.split(" ")[1].strip(":"))

def possible_game_ids(games):
    return [game_id(game) for game in games if is_possible(game)]

with open("input") as f:
    games = [line.strip() for line in f.readlines()]

print(sum(possible_game_ids(games)))
