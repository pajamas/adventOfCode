def first_number(line):
    for i in range(len(line)):
        if line[i].isdigit():
            return line[i]
    return None

def last_number(line):
    for i in range(len(line)-1, -1, -1):
        if line[i].isdigit():
            return line[i]
    return None

def calibration_number(line):
    return int(first_number(line) + last_number(line))

def calibration_sum(lines):
    return sum([calibration_number(line) for line in lines])

with open("input") as f:
    lines = f.readlines()

print(calibration_sum(lines))
