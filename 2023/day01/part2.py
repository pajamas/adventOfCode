def spelled_out_numbers_in_line(line):
    numbers = ["one", "two", "three",
               "four", "five", "six",
               "seven", "eight", "nine"]
    numbers_in_line = []
    for i in range(len(line)):
        for n_i, number in enumerate(numbers):
            if line[i:].startswith(number):
                numbers_in_line.append((str(n_i+1), i))
    return numbers_in_line

def second_item(t):
    return t[1]

def first_number(line):
    number_indices = spelled_out_numbers_in_line(line)

    for i in range(len(line)):
        if line[i].isdigit():
            number_indices.append((line[i], i))
            break

    return min(number_indices, key=second_item)[0]

def last_number(line):
    number_indices = spelled_out_numbers_in_line(line)

    for i in range(len(line)-1, -1, -1):
        if line[i].isdigit():
            number_indices.append((line[i], i))
            break

    return max(number_indices, key=second_item)[0]

def calibration_number(line):
    return int(first_number(line) + last_number(line))

def calibration_sum(lines):
    return sum(calibration_number(line) for line in lines)

with open("input") as f:
    lines = [line.strip() for line in f.readlines()]

print(calibration_sum(lines))
