with open("input","r") as f:
    lines = f.readlines()

lines = [int(line.strip()) for line in lines]


def findNums(lines):
    l = len(lines)
    for i in range(l):
        for j in range(l):
            if lines[i]+lines[j] == 2020:
                return lines[i]*lines[j]

print(findNums(lines))


def findNums2(lines):
    l = len(lines)
    for i in range(l):
        for j in range(l):
            for k in range(l):
                if lines[i]+lines[j]+lines[k] == 2020:
                    return lines[i]*lines[j]*lines[k]

print(findNums2(lines))
