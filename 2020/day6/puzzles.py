with open("input","r") as f:
    text = f.read()

text = text.split("\n\n")

def count(text: list):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    num = 0
    for answers in text:
        for char in alphabet:
            if char in answers: num += 1
    return num

print(count(text))

def count_if_all(text: list):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    num = 0
    for i in range(len(text)):
        text[i] = answers_to_list(text[i])
        for char in alphabet:
            if is_char_in_all(text[i],char):
                num += 1
    return num

def answers_to_list(answers: str):
    if answers[-1] == "\n":
        answers = answers[:-1]
    answerslist = answers.split("\n")
    return answerslist

def is_char_in_all(answers,char):
    for answer in answers:
        if not char in answer:
            return False
    return True

print(count_if_all(text))
