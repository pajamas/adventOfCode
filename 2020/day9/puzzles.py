with open("input","r") as f:
    lines = f.readlines()

lines = [int(line.strip()) for line in lines]

def find_num(lines: list) -> int:
    previous = lines[:25]
    for i in range(25,len(lines)):
        sums = []
        for j in previous:
            for k in previous:
                sums.append(j+k)
        if lines[i] not in sums:
            return(lines[i])
        previous = previous[1:]+[lines[i]]

def encryption_weakness(lines: list, num: int) -> int:
    numset = {}
    i=0
    while i<len(lines):
        numset[i] = lines[i]
        if sum(numset.values()) == num:
            return min(numset.values()) + max(numset.values())
        elif sum(numset.values()) > num:
            i = min(numset.keys())
            numset = {}
        i += 1
        
print(find_num(lines))
print(encryption_weakness(lines,find_num(lines)))
