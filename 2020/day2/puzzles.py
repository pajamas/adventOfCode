with open("input","r") as f:
    lines = f.readlines()

lines = [line.strip().split(" ") for line in lines]

for i,line in enumerate(lines):
    lines[i] = [line[0].split("-"), line[1][0], line[2]]
    lines[i][0] = [int(lines[i][0][0]),int(lines[i][0][1])]

def check(line):
    minn = line[0][0]
    maxn = line[0][1]
    letter = line[1]
    inpw = line[2].count(letter)
    if minn<=inpw<=maxn:
        return True
    return False

def check2(line):
    pos1 = line[0][0]
    pos2 = line[0][1]
    letter = line[1]
    pw = line[2]
    if pw[pos1-1]==letter and not pw[pos2-1]==letter:
        return True
    if pw[pos2-1]==letter and not pw[pos1-1]==letter:
        return True
    return False

amount = 0
amount2 = 0
for line in lines:
    if check(line):
        amount += 1
    if check2(line):
        amount2 += 1
print(amount)
print(amount2)
