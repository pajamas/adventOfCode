with open("input","r") as f:
    text = f.read()

def makePassports(text):
    passports = text.split("\n\n")

    passports = [passport.replace("\n"," ").split(" ") for passport in passports]
    del passports[-1][-1]

    for i in range(len(passports)):
        for j in range(len(passports[i])):
            split = passports[i][j].split(":")
            dkey = split[0]
            dval = split[1]
            passports[i][j] = {dkey:dval}
        d = {}
        for dc in passports[i]:
            d.update(dc)
        passports[i] = d

    return passports

def validAmount(passports,puzzleNum):
    amount = 0
    for passport in passports:
        if puzzleNum==1:
            if validate(passport):
                amount += 1
        if puzzleNum==2:
            if validate2(passport):
                amount += 1
    return amount

def validate(passport):
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]   
    for field in required:
        if not field in passport:
            return False
    return True

def validate2(passport):
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]   
    for field in required:
        if not field in passport:
            return False
    byr = passport["byr"]
    if (not 1920<=int(byr)<=2002) or len(byr)!=4:
        return False 
    iyr = passport["iyr"]
    if not 2010<=int(iyr)<=2020:
        return False
    eyr = passport["eyr"]
    if not 2020<=int(eyr)<=2030:
        return False
    hgt = passport["hgt"]
    un = hgt[-2] + hgt[-1]
    if not(un=="in" or un=="cm"): return False
    if un=="in":
        if not 59<=int(hgt[:-2])<=76:
            return False
    if un=="cm":
        if not 150<=int(hgt[:-2])<=193:
            return False
    hcl = passport["hcl"]
    if len(hcl)!=7 or hcl[0]!="#" or len([True for char in hcl[1:] if char in "1234567890abcdef"])!=6:
        return False
    ecl = passport["ecl"]
    if not ecl in ["amb","blu","brn","gry","grn","hzl","oth"]:
        return False
    pid = passport["pid"]
    if not (len(pid)==9 and pid.isnumeric()): return False
    return True

print(validAmount(makePassports(text),1))
print(validAmount(makePassports(text),2))
