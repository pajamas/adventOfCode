with open("input","r") as f:
    lines = f.readlines()

lines = [line.strip() for line in lines]

def calcTrees(slope: tuple):
    trees = 0
    x = 0
    y = 0
    while y<len(lines):
        if lines[y][x % len(lines[0])] == "#":
            trees += 1
        x += slope[0]
        y += slope[1]
    return(trees)

print(calcTrees((3,1)))

slopes = [(1,1),(3,1),(5,1),(7,1),(1,2)]

total = 1
for slope in slopes:
    total *= calcTrees(slope)

print(total)
