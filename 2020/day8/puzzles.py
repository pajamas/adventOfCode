from copy import deepcopy

with open("input","r") as f:
    lines = f.readlines()

lines = [[line.strip().split(" "),0] for line in lines]

def boot(lines: list, printbroken: int) -> int:
    acc = 0
    i = 0
    while i<len(lines):
        if lines[i][1] == 1:
            if printbroken:
                print("acc after one loop:",acc)
            return 0
        if lines[i][0][0] == "nop":
            lines[i][1] = 1
            i += 1
            continue
        if lines[i][0][0] == "acc" or lines[i][0][0] == "jmp":
            op = lines[i][0][0]
            num = int(lines[i][0][1][1:])
            if "-" in lines[i][0][1]:
                num = -num
            if op == "acc":
                acc += num
                lines[i][1] = 1
                i += 1
                continue
            if op == "jmp":
                lines[i][1] = 1
                i += num
    print("acc after correct boot:",acc)
    return 1

def fix(lines: list) -> int:
    acc = 0
    for i,line in enumerate(lines):
        newlines = deepcopy(lines)
        if line[0][0] == "jmp":
            newlines[i][0][0] = "nop"
        elif line[0][0] == "nop":
            newlines[i][0][0] = "jmp"
        if boot(newlines,0):
            return 1

# not fixed
linescopy = deepcopy(lines)
boot(linescopy,1)

#fixed
linescopy = deepcopy(lines)
fix(linescopy)
