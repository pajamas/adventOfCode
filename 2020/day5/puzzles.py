with open("input","r") as f:
    lines = f.readlines()

def decode_seat(codedseat):
    # what row
    lower = 0
    upper = 127
    for char in codedseat[0:8]:
        if char == "F":
            upper -= round((upper-lower+1)/2)
        elif char == "B":
            lower += round((upper-lower+1)/2)
    # what seat
    lower2 = 0
    upper2 = 7
    for char in codedseat[7:11]:
        if char == "L":
            upper2 -= round((upper2-lower2+1)/2)
        elif char == "R":
            lower2 += round((upper2-lower2+1)/2)
    return (lower,lower2)

def calculate_id(seat: tuple):
    seat_id = seat[0]*8+seat[1]
    return seat_id

ids = []
for codedseat in lines:
    ids.append(calculate_id(decode_seat(codedseat)))

print(max(ids))

def find_missing_id(ids):
    for seat_id in ids:
        if (not seat_id+1 in ids) and seat_id+2 in ids:
            return seat_id+1

print(find_missing_id(ids))
