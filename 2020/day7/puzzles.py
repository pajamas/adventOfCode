with open("input","r") as f:
    lines = f.readlines()

def lines_into_dict(lines: list) -> dict:
    colors = {}
    for i,line in enumerate(lines):
        lines[i] = lines[i].strip("\n").split(" contain ")
        container = lines[i][0][:-5]
        contains = lines[i][1]
        contains = contains.strip(".").split(", ")
        for j in range(len(contains)):
            contains[j] = contains[j][2:]
            if "bags" in contains[j]:
                contains[j] = contains[j].replace(" bags","")
            elif "bag" in contains[j]:
                contains[j] = contains[j].replace(" bag","")
            if "other" in contains[j]:
                contains = []
        colors[container] = contains
    return colors
    
def list_child_colors(colors: dict, color):
    colorsToAdd = []
    if len(colors[color])==0:
        return []
    else:
        for childColor in colors[color]:
            colorsToAdd += [childColor]
            c = list_child_colors(colors,childColor)
            colorsToAdd += c
        return colorsToAdd

d = lines_into_dict(lines.copy())
newd = d.copy()
amount = 0
for color in list(d.keys()):
    colorsToAdd = list_child_colors(d, color)
    for col in colorsToAdd:
        if not col in d[color]:
            newd[color] = newd[color] + [col]
    if "shiny gold" in newd[color]:
        amount += 1

print(amount)


def lines_into_dict_with_nums(lines: list) -> dict:
    colors = {}
    for i,line in enumerate(lines):
        lines[i] = lines[i].strip("\n").split(" contain ")
        container = lines[i][0][:-5]
        contains = lines[i][1]
        contains = contains.strip(".").split(", ")
        for j in range(len(contains)):
            if "other" in contains[j]:
                contains = []
                continue
            if "bags" in contains[j]:
                contains[j] = contains[j].replace(" bags","")
            elif "bag" in contains[j]:
                contains[j] = contains[j].replace(" bag","")
            amount = contains[j][0]
            contains[j] = [contains[j][2:],int(amount)]
        colors[container] = contains
    return colors


def child_nums(colors: dict, color, num):
    insideNum = 0
    if len(colors[color])==0:
        return 0
    else:
        for childColor in colors[color]:
            insideNum += num*childColor[1]
            c = child_nums(colors,childColor[0],childColor[1])
            c2 = num * c
            insideNum += c2
        return insideNum

d2 = lines_into_dict_with_nums(lines.copy())

print(child_nums(d2, "shiny gold",1))
